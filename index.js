import chalk from 'chalk';
import cTable from 'console.table';

const log = console.log;
let totalCP = 0;
let criticals = 0;
let specials = 0;

// ** CONFIG ** //
const nTurns = 7;
const nCombats = 1000;
const nSkillLevel = 70;
// ** END CONFIG ** //

log(chalk.blue(`SIMULATION STARTED - ${nCombats} combats, ${nTurns} turns each, skill level: ${nSkillLevel}`));

const r = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

const combat = () => {
  let t = nTurns;
  let CP = 0;
  while (t > 0) {
    const attackRoll = [r(0, 9), r(0, 9)];
    const defenseRoll = [r(0, 9), r(0, 9)];
    const attack = parseInt(attackRoll[0] + '' + attackRoll[1]);
    const defense = parseInt(defenseRoll[0] + '' + defenseRoll[1]);
    let greatSuccess = '';

    if ((attackRoll[0] === attackRoll[1] || attack === 0) && attack <= nSkillLevel) { // critical hit
      CP += 2;
      criticals++;
      greatSuccess += `Critical attack! ${attack} `;
    }
    else if (attack <= nSkillLevel / 2) { // special hit
      CP++;
      specials++;
    }

    if ((defenseRoll[0] === defenseRoll[1] || defense === 0) && defense <= nSkillLevel) { // critical defense
      CP += 2;
      criticals++;
      greatSuccess += `Critical defense! ${defense} `;
    }
    else if (defense <= nSkillLevel / 2) { // special defense
      CP++;
      specials++;
    }

    log(chalk.yellow(`Turn ${nTurns - t + 1} - Attack: ${attack} - Defense: ${defense} - CP: ${CP} ${greatSuccess}`));
    t--;
  }
  log(chalk.red(`Combat ended: ${nTurns} turns, CP earned: ${CP}`));

  return CP;
};

const simulation = () => {
  let c = nCombats;
  while (c > 0) {
    totalCP += combat();
    c--;
  }
};

simulation();

const values = [
  ['combats', nCombats],
  ['turns', nTurns],
  ['skill level', nSkillLevel],
  ['total CP', totalCP],
  ['CP per combat', totalCP / nCombats],
  ['CP per turn', totalCP / (nCombats * nTurns)],
  ['criticals', criticals],
  ['specials', specials],
  // crits % per combat
  ['criticals % per combat', criticals / nCombats],
  // specials % per combat
  ['specials % per combat', specials / nCombats],
];

console.table(['name', 'value'], values);
log(chalk.blue(`SIMULATION ENDED - ${nCombats} combats, ${nTurns} turns each, skill level: ${nSkillLevel}`));